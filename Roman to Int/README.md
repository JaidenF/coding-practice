Here I did the Roman to integer problem, where if given a roman numeral it would be converted into the correct integer.

The .py file is the one I solved on my own while the .c file is one that a friend of mine showed me.

Link to leetcode problem:
https://leetcode.com/problems/roman-to-integer/
