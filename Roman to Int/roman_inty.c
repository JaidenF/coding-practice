int romanToInt(char * s){
   int temp;
   int sum = 0;
   for (int i = 0; i < strlen(s); i++)
   {
      temp = 0;
      switch(s[i])
      {
         case 'I':
            if (s[i+1] == 'V')
            {
               temp = 4;
               i++;
            }

            else if (s[i+1] == 'X')
            {
               temp = 9;
               i++;
            }

            else
            {
               temp = 1;
            }
            break;
         case 'V':
            temp = 5;
            break;
         case 'X':

            if (s[i+1] == 'L')
            {
               temp = 40;
               i++;
            }

            else if (s[i+1] == 'C')
            {
               temp = 90;
               i++;
            }

            else
            {
               temp = 10;
            }

            break;
         case 'L':
            temp = 50;
            break;
         case 'C':
            if (s[i+1] == 'D')
            {
               temp = 400;
               i++;
            }
            else if (s[i+1] == 'M')
            {
               temp = 900;
               i++;
            }
            else
            {
               temp = 100;
            }
            break;
         case 'D':
            temp = 500;
            break;
         case 'M':
            temp = 1000;
            break;
      }
      sum += temp;
   }
   return sum;
}
