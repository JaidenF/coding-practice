def longestCommonPrefix(strs):
    prefix = ""
    for i in range(len(strs) - 1):
        temp = ""
        if len(strs[i]) < len(strs[i + 1]):
            temp = strs[i]
        else:
            temp = strs[i + 1]

        for j in range(len(temp)):
            if strs[i][j] == strs[i + 1][j]:
                prefix = strs[i][j]
            else:
                break

    return prefix


a = ["word", "something", "title", "stuff", "accept"]

print(longestCommonPrefix(a))
