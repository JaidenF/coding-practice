class Solution:
    def twoSum(self, nums, target):
        for i in range(len(nums)):
            for j in range(i + 1, (len(nums))):
                sum = nums[i] + nums[j]
                if sum == target:
                    return [i, j]

        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """


example = Solution()

print(example.twoSum([0, 4, 1, 5, 0], 9))
