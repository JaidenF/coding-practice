class Solution(object):
    def isPalindrome(self, x):
        string = str(x)
        rev_string = string[::-1]
        if string == rev_string:
            return True
        else:
            return False


example = Solution()

print(
    example.isPalindrome(
        1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111112
    )
)
